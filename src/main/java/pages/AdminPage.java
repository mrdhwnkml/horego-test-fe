package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AdminPage {

    private WebDriver driver;
    private By addBtn = By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']");

    private By role = By.xpath("(//div[contains(@class, 'oxd-select-text')])[1]");

    private By status = By.xpath("(//div[contains(@class, 'oxd-select-text')])[4]");

    private By Empname = By.xpath("//input[@placeholder='Type for hints...']");
    private By Username = By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]");
    private By Password = By.xpath("(//input[@type='password'])[1]");
    private By ConfirmPassword = By.xpath("(//input[@type='password'])[2]");

    private By SaveBtn = By.xpath("//button[@type='submit']");

    private By popupSuccess = By.xpath("//div[@class='oxd-toast oxd-toast--success oxd-toast-container--toast']");


    public AdminPage(WebDriver driver) {
        this.driver = driver;
    }


    public void ClickaddBtn() {
        driver.findElement(addBtn).click();


    }
    public void role() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(role).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement dropdown = driver.findElement(By.xpath("(//div[contains(@class, 'oxd-select-text')])[1]"));

        // Create Actions object
        Actions actions = new Actions(driver);

        // Move to the dropdown element and perform scroll down
        actions.moveToElement(dropdown).sendKeys(org.openqa.selenium.Keys.ARROW_DOWN).build().perform();
        actions.sendKeys(Keys.RETURN).build().perform();

    }
    public void status() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(status).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement dropdown = driver.findElement(By.xpath("(//div[contains(@class, 'oxd-select-text')])[4]"));

        // Create Actions object
        Actions actions = new Actions(driver);

        // Move to the dropdown element and perform scroll down
        actions.moveToElement(dropdown).sendKeys(org.openqa.selenium.Keys.ARROW_DOWN).build().perform();
        actions.sendKeys(Keys.RETURN).build().perform();

    }

    public void EmpName(String empName) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(Empname).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(Empname).sendKeys(empName);

        //No Need
    /*
        By locators = By.xpath("//div[@class='oxd-autocomplete-text-input' and @data-v-75e744cd='']/input");
        int maxWaitTimeSeconds = 1000;
        // Create WebDriverWait instance
        WebDriverWait wait = new WebDriverWait(driver, maxWaitTimeSeconds);
    */


        try {
            Thread.sleep(2000); // Menunggu 2 detik
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //No Need
    /*
        try {
        // Wait for the element to be visible
       WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locators));
       } catch (Exception e) {
            // Handle the exception if the element is not visible within the specified time
           e.printStackTrace();
        }
     */

        WebElement dropdown = driver.findElement(By.xpath("//input[@placeholder='Type for hints...']"));

        // Create Actions object
        Actions actions = new Actions(driver);

        // Move to the dropdown element and perform scroll down
        actions.moveToElement(dropdown).sendKeys(org.openqa.selenium.Keys.ARROW_DOWN).build().perform();

        actions.sendKeys(Keys.RETURN).build().perform();
    }

    public void username(String username) {
        driver.findElement(Username).sendKeys(username);

    }

    public void password(String password) {
        driver.findElement(Password).sendKeys(password);

    }

    public void confirmPassword(String confirmPassword) {
        driver.findElement(ConfirmPassword).sendKeys(confirmPassword);
    }

    public void saveBtn() {
        driver.findElement(SaveBtn).click();
    }

    public void PopupSuccess() {
        driver.findElement(popupSuccess).isDisplayed();
    }


}
