package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {

    private WebDriver driver;
    private By subMenu = By.xpath("//a[@class='oxd-main-menu-item' and contains(span, 'Admin')]");


    public DashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ClicksubMenu() {
        driver.findElement(subMenu).click();
    }
}
