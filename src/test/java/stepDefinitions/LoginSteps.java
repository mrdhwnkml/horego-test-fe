package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.Assert.assertTrue;
import BaseTest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pages.DashboardPage;
import pages.LoginPage;


public class LoginSteps extends BaseTest {

    private LoginPage loginPage = new LoginPage(driver);

    @Given("User is on the login pages")
    public void userIsOnLoginPage() {

        driver.manage().window().maximize();
        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
        By locator = By.name("username");
        int maxWaitTimeSeconds = 10;

        // Create WebDriverWait instance
        WebDriverWait wait = new WebDriverWait(driver, maxWaitTimeSeconds);

        try {
            // Wait for the element to be visible
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

            // Now you can interact with the visible element
          //  element.click();
        } catch (Exception e) {
            // Handle the exception if the element is not visible within the specified time
            e.printStackTrace();
        }
    }


    @When("User enters valid username and password")
    public void userEntersValidCredentials() {
        loginPage.enterUsername("Admin");
        loginPage.enterPassword("admin123");
    }

    @When("User clicks on the login button")
    public void userClicksOnLoginButton() {
        loginPage.clickLoginButton();
    }

    @Then("User should be logged in successfully")
    public void userLoggedInSuccessfully() {
        String actualUrl = driver.getCurrentUrl();
        String expectedUrl = "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index";
        Assert.assertEquals(actualUrl, expectedUrl, "Login did not redirect to the expected URL.");
        driver.quit();
    }



}
