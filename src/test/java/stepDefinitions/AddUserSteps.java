package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pages.AdminPage;
import pages.DashboardPage;
import java.util.Random;
import java.util.UUID;

import static BaseTest.BaseTest.driver;


public class AddUserSteps {

    private DashboardPage dashboardPage = new DashboardPage(driver);;
    private AdminPage adminPage = new AdminPage(driver);

    @When("User click on Admin")
    public void userClickOnAdmin() {
        By locators = By.xpath("//a[@class='oxd-main-menu-item' and contains(span, 'Admin')]");
        int maxWaitTimeSeconds = 10;

        // Create WebDriverWait instance
        WebDriverWait wait = new WebDriverWait(driver, maxWaitTimeSeconds);

        try {
            // Wait for the element to be visible
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locators));

        } catch (Exception e) {
            // Handle the exception if the element is not visible within the specified time
            e.printStackTrace();
        }
        dashboardPage.ClicksubMenu();
    }


    @And("User click on +Add")
    public void userClickOnAdd() {
        By locators = By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']");
        int maxWaitTimeSeconds = 10;
        // Create WebDriverWait instance
        WebDriverWait wait = new WebDriverWait(driver, maxWaitTimeSeconds);

        try {
            // Wait for the element to be visible
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locators));

        } catch (Exception e) {
            // Handle the exception if the element is not visible within the specified time
            e.printStackTrace();
        }
        adminPage.ClickaddBtn();

    }

    @And("User filled the form")
    public void userFilledTheForm() {
        adminPage.role();
        adminPage.status();
        adminPage.EmpName("a");
        String randomString = RandomStringUtils.randomAlphabetic(8);
        Random random = new Random();
        int randomInt = random.nextInt(10);
        //String randomPassword = UUID.randomUUID().toString();
        adminPage.username(randomString);
        adminPage.password(randomString+randomInt);
        adminPage.confirmPassword(randomString+randomInt);
    }

    @And("User click on save")
    public void userClickOnSave() {
        adminPage.saveBtn();

    }

    @Then("User should be success add a user")
    public void userShouldBeSuccessAddAUser() {
        adminPage.PopupSuccess();
        driver.quit();
    }

    @Then("User should be logged in successfully.")
    public void userShouldBeLoggedInSuccessfully() {
        String actualUrl = driver.getCurrentUrl();
        String expectedUrl = "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index";
        Assert.assertEquals(actualUrl, expectedUrl, "Login did not redirect to the expected URL.");

    }

}
