Feature: Add User Feature

  Scenario: User success add a user
    Given User is on the login pages
    When User enters valid username and password
    And User clicks on the login button
    Then User should be logged in successfully.
    When User click on Admin
    And User click on +Add
    And User filled the form
    And User click on save
    Then User should be success add a user
