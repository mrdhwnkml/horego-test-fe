Feature: Login Feature

  Scenario: User logs in with valid credentials
    Given User is on the login pages
    When User enters valid username and password
    And User clicks on the login button
    Then User should be logged in successfully
